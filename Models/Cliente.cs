using System;
using System.ComponentModel.DataAnnotations;

namespace DemoApi.Models
{
    public class Cliente 
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string nombre { get; set; }
        [Required]
        public string apellido { get; set; }
        [Required]
        public string tipoDocumento { get; set; }
        [Required]
        public int nroDocumento { get; set; }
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Invalid Email")]
        public string email { get; set; }
        public string telefono { get; set; }
        public DateTime ? fechaNacimiento { get; set; }
    }
}
