using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoApi.Data;
using DemoApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DemoApi.Controllers
{
    [ApiController]
    [ApiVersion("1.0", Deprecated = true)]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Produces("application/json", "application/xml")]
    public class ClienteController : ControllerBase
    {
        private readonly ClienteAppContext _context;

        // initiate database context  
        public ClienteController(ClienteAppContext context)
        {
            _context = context;
        }

        [HttpGet("HolaMundo")]
        [MapToApiVersion("1.0")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(200)]
        public ActionResult<string> GetHolaMundo()
        {
            return Ok("Hola Mundo!!!");
        }

        // GET api/cliente
        [HttpGet]
        [MapToApiVersion("1.0")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(200)]
        public ActionResult<IQueryable<Cliente>> Get()
        {
            return Ok(_context.Clientes);
        }

        // GET api/cliente/5
        [HttpGet("{id}")]
        [MapToApiVersion("1.0")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public ActionResult<Cliente> GetById([FromRoute] int id)
        {
            // filter client records by client id  
            var item = _context.Clientes.FirstOrDefault(t => t.id == id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        [HttpGet("Sorting")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public ActionResult<IQueryable<Cliente>> GetSorting([FromQuery] string ordenApellido)
        {
            IQueryable<Cliente> products;
            switch (ordenApellido)
            {
                case "desc":
                    products = _context.Clientes.OrderByDescending(p => p.apellido);
                    break;
                case "asc":
                    products = _context.Clientes.OrderBy(p => p.apellido);
                    break;
                case "":
                case null:
                    products = _context.Clientes;
                    break;
                default:
                    return BadRequest();
            }

            return Ok(products);
        }

        [HttpGet("Paging")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(200)]
        public ActionResult<IQueryable<Cliente>> GetPaging([FromQuery] int numeroPag = 1, [FromQuery] int cantidadReg = 5)
        {
            var clientes = (from p in _context.Clientes.OrderBy(m => m.id) select p).AsQueryable();
            var items = clientes.Skip((numeroPag - 1) * cantidadReg).Take(cantidadReg).ToList().AsQueryable();
            return Ok(items);
        }

        [HttpGet("Searching")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public ActionResult<IQueryable<Cliente>> GetSearching([FromQuery] string apellidoBuscar)
        {
            if (apellidoBuscar == null || apellidoBuscar == "")
            {
                return BadRequest();
            }

            var clientes = _context.Clientes.Where(p => p.apellido.StartsWith(apellidoBuscar)).AsQueryable();
            return Ok(clientes);
        }

        /// <summary>
        /// Crear cliente.
        /// </summary>
        /// <remarks>
        /// Ejemplo:
        ///
        ///     POST /cliente
        ///     {
        ///         "id": 0,
        ///         "nombre": "Gabriel",
        ///         "apellido": "Lavini",
        ///         "tipoDocumento": "DNI",
        ///         "nroDocumento": 25975549,
        ///         "email": "gabriel.lavini@cencosud.com",
        ///         "telefono": "+54 11 4733-1000",
        ///         "fechaNacimiento": "1977-05-15"
        ///     }
        ///
        /// </remarks>
        /// <param name="item"></param>
        /// <returns>Un nuevo cliente creado</returns>
        /// <response code="201">Retorna si pudo crear al cliente</response>
        /// <response code="400">Retorna si el valor del item es null</response>   
        [HttpPost]
        [MapToApiVersion("1.0")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public IActionResult Post([FromBody] Cliente item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // set bad request if client data is not provided in body  
            if (item == null)
            {
                return BadRequest();
            }

            try
            {
                _context.Clientes.Add(item);
                _context.SaveChanges(true);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
            return StatusCode(StatusCodes.Status201Created, "Cliente agregado correctamente.");
        }

        // PUT api/cliente/5
        [HttpPut("{id}")]
        [MapToApiVersion("1.0")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult Put(int id, [FromBody] Cliente item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null || id == 0 || item.id != id)
            {
                return BadRequest();
            }

            try
            {
                _context.Clientes.Update(item);
                _context.SaveChanges(true);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }

            return Ok("Cliente modificado satisfactoriamente.");
        }

        /// <summary>
        /// Borrar cliente.
        /// </summary>
        /// <param name="id"></param>  
        [HttpDelete("{id}")]
        [MapToApiVersion("1.0")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult Delete(int id)
        {
            var cliente = _context.Clientes.FirstOrDefault(t => t.id == id);
            if (cliente == null)
            {
                return NotFound("No se encontró registro.");
            }

            try
            {
                _context.Clientes.Remove(cliente);
                _context.SaveChanges(true);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }

            return Ok("Cliente borrado satisfactoriamente.");
        }
    }
}
